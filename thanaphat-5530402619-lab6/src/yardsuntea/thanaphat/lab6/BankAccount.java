package yardsuntea.thanaphat.lab6;

import javax.swing.*;
import java.awt.*;

public class BankAccount {

	public static void main(String[] args) {
		
		createAndShowGUI();		
		
	}

	public static void createAndShowGUI() {
		// FRAME
		JFrame frame = new JFrame("Simple Bank Account");

		// PANEL
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());

		// LABEl
		JLabel prebalance = new JLabel("Previous Balance:");
		JLabel amount = new JLabel("Amount:");
		JLabel curbalance = new JLabel("Current Balance:");

		// BUTTON
		JButton wihtdrawB = new JButton("Wihtdraw");
		JButton depositB = new JButton("Deposit");
		JButton exitB = new JButton("Exit");

		// TEXT
		JTextField textPrebalance = new JTextField("1000", 12);
		JTextField textAmount = new JTextField(12);
		JTextField textCurbalance = new JTextField(12);

		JPanel labelPrebalance = new JPanel();
		JPanel labelAmount = new JPanel();
		JPanel labelCurbalance = new JPanel();
		JPanel textPb = new JPanel();
		JPanel textA = new JPanel();
		JPanel textCb = new JPanel();

		labelPrebalance.add(prebalance);
		labelAmount.add(amount);
		labelCurbalance.add(curbalance);
		textPb.add(textPrebalance);
		textA.add(textAmount);
		textCb.add(textCurbalance);

		JPanel panelMix = new JPanel();
		panelMix.setLayout(new BorderLayout());
		panel.add(panelMix);

		JPanel panelMix1 = new JPanel();
		panelMix1.setLayout(new BorderLayout());
		panelMix.add(panelMix1, BorderLayout.NORTH);

		JPanel panelMix2 = new JPanel();
		panelMix.add(panelMix2, BorderLayout.SOUTH);

		JPanel panelLine1 = new JPanel();
		panelLine1.setLayout(new BorderLayout());
		panelLine1.add(labelPrebalance, BorderLayout.WEST);
		panelLine1.add(textPb, BorderLayout.EAST);
		panelMix1.add(panelLine1, BorderLayout.NORTH);

		JPanel panelLine2 = new JPanel();
		panelLine2.setLayout(new BorderLayout());
		panelLine2.add(labelAmount, BorderLayout.WEST);
		panelLine2.add(textA, BorderLayout.EAST);
		panelMix1.add(panelLine2, BorderLayout.CENTER);

		JPanel panelLine3 = new JPanel();
		panelLine3.setLayout(new BorderLayout());
		panelLine3.add(labelCurbalance, BorderLayout.WEST);
		panelLine3.add(textCb, BorderLayout.EAST);
		panelMix1.add(panelLine3, BorderLayout.SOUTH);

		JPanel panelButton = new JPanel();
		panelButton.add(wihtdrawB);
		panelButton.add(depositB);
		panelButton.add(exitB);
		panelMix2.add(panelButton);

		frame.add(panel);
		frame.setLocation(400, 200);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.pack();
		frame.setVisible(true);

	}

}
