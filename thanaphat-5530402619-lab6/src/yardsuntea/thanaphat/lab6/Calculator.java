package yardsuntea.thanaphat.lab6;

import java.awt.*;
import javax.swing.*;

public class Calculator {

	public static void main(String[] args) {
		JFrame frame = new JFrame("calculator");
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());
		JTextField text = new JTextField(22);
		JPanel textTop = new JPanel();
		textTop.add(text);

		JButton[] numB = new JButton[10]; // Create new JButton for keep number 0-9
		for (int i = 0; i < 10; i++) {
			if (i < 9) {
				numB[i] = new JButton(" " + (i + 1));
			} else {
				numB[i] = new JButton(" " + 0);
			}
		}

		JButton[] symbolB = new JButton[6]; // Create new JButton for keep symbol
		symbolB[0] = new JButton("+");
		symbolB[1] = new JButton("-");
		symbolB[2] = new JButton("*");
		symbolB[3] = new JButton("/");
		symbolB[4] = new JButton("%");
		symbolB[5] = new JButton("=");

		JPanel[] numPanel = new JPanel[10]; 
		for (int i = 0; i < 10; i++) {		// Loop numPanel for keep number 0-9 
			numPanel[i] = new JPanel();
			numPanel[i].add(numB[i]);
			numPanel[i].setBackground(Color.gray); // set color for panel
		}

		JPanel[] symbolPanel = new JPanel[6];
		for (int i = 0; i < 6; i++) {		// Loop symbolPanel for keep symbol
			symbolPanel[i] = new JPanel();
			symbolPanel[i].add(symbolB[i]);
			symbolPanel[i].setBackground(Color.pink); // set color for panel
		}

		JPanel[] numPanel1 = new JPanel[10];
		for (int i = 0; i < 10; i++) {		
			numPanel1[i] = new JPanel();
			numPanel1[i].add(numPanel[i]);
		}

		JPanel[] symbolPanel1 = new JPanel[6];
		for (int i = 0; i < 6; i++) {
			symbolPanel1[i] = new JPanel();
			symbolPanel1[i].add(symbolPanel[i]);
		}

		JPanel mixP = new JPanel(); // Loop MixP for mix numPanel1 and symbolPanel1
		mixP.setLayout(new GridLayout(4, 4));
		for (int i = 0; i < 10; i++) {
			mixP.add(numPanel1[i]);
		}
		for (int i = 10; i < 16; i++) {
			mixP.add(symbolPanel1[i - 10]);
		}

		panel.add(textTop, BorderLayout.NORTH);
		panel.add(mixP, BorderLayout.SOUTH);

		frame.add(panel);
		frame.pack();
		frame.setLocation(400, 200);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);

	}

}
